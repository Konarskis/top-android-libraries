<H2>UI</H2>

Alerter<br />
https://github.com/Tapadoo/Alerter<br />
<br/>
Button<br />
https://github.com/dmytrodanylyk/circular-progress-button<br />
https://github.com/dmytrodanylyk/android-process-button<br />
<br />
Layout<br />
https://github.com/etsy/AndroidStaggeredGrid<br />
<br />
Recyclerview animations<br />
https://github.com/wasabeef/recyclerview-animators<br />
<br />
Text drawable<br />
https://github.com/amulyakhare/TextDrawable<br />
<br />
Material code input<br />
https://github.com/glomadrian/material-code-input<br />
<br />
Email autocomplete textview<br />
https://github.com/tasomaniac/EmailAutoCompleteTextView<br />
<br />
Circle imageview<br />
https://github.com/hdodenhof/CircleImageView<br />
<br />
Image cropping activity<br />
https://github.com/jdamcd/android-crop<br />
<br />
Contextual menu<br />
https://github.com/Yalantis/Context-Menu.Android<br />
<br />
Set of fonts<br />
https://github.com/vsvankhede/easyfonts<br />
<br />
Slider activity<br />
https://github.com/r0adkll/Slidr<br />
<br />
Material dialogs<br />
https://github.com/afollestad/material-dialogs<br />
<br />
Spotlight for feature explanations<br />
https://github.com/TakuSemba/Spotlight<br />